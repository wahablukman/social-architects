import axios from 'axios';
import {
  GET_ALL_PROFILES,
  GET_PROFILE,
  IS_LOADING,
  GET_SUCCESS,
  GET_ERRORS,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  CREATE_OR_UPDATE_PROFILE,
  UPDATE_AVATAR,
  UPDATE_AUTH_INSIDE_PROFILE,
  UPDATE_AUTH_FROM_PROFILE
} from './types';

// get all profiles, if a user is registered YET do NOT have a profile then it won't be returned
export const getAllProfiles = () => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.get('/api/profile/all');
    dispatch({
      type: GET_ALL_PROFILES,
      payload: response.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(
      getErrorMessage(
        err.response.data.message ? err.response.data.message : err
      )
    );
  }
};

// create profile (or update if it already exists)
export const createOrUpdateProfile = profileData => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.post('/api/profile', profileData);
    // return console.log(response.data);
    dispatch({
      type: CREATE_OR_UPDATE_PROFILE,
      payload: response.data.updateProfile
    });
    //change auth stuffs on user object inside profile
    dispatch({
      type: UPDATE_AUTH_INSIDE_PROFILE,
      payload: response.data.updateUser
    });
    //change auth stuffs on user object inside auth
    dispatch({
      type: UPDATE_AUTH_FROM_PROFILE,
      payload: response.data.updateUser
    });
    dispatch(clearErrors());
    dispatch(getSuccessMessage('Profile updated succesfully!'));
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return console.log(err);
  }
};

//update avatar
export const updateAvatar = avatarData => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const uploadToCloudinary = await axios.post(
      '/api/profile/profile-picture',
      avatarData
    );
    dispatch(clearErrors);
    dispatch({
      type: UPDATE_AVATAR,
      payload: uploadToCloudinary.data
    });
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err));
  }
};

// get current user's profile from req.user(logged in user)
export const getCurrentUserProfile = () => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.get(`/api/profile/myprofile`);
    // return console.log(response.data);
    dispatch({
      type: GET_PROFILE,
      payload: response.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getSuccessMessage(`Let's create a profile here!`));
  }
};

// get a profile by parameter profile ID
export const getProfile = paramData => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.get(`/api/profile/${paramData}`);
    // return console.log(response.data);
    dispatch({
      type: GET_PROFILE,
      payload: response.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(
      getErrorMessage(
        err.response.data.message ? err.response.data.message : err
      )
    );
  }
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const getErrorMessage = errorMessage => dispatch => {
  dispatch({
    type: GET_ERRORS,
    payload: errorMessage
  });
  setTimeout(() => {
    dispatch(clearErrors());
  }, 3000);
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
