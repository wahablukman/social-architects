import axios from 'axios';
import setAuthToken from '../../utils/setAuthToken';
import decode from 'jwt-decode';
import {
  SET_CURRENT_USER,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_SUCCESS,
  CLEAR_SUCCESS,
  IS_LOADING,
  CLEAR_PROFILE,
  CLEAR_PLAYLIST,
  CLEAR_MESSAGE
} from './types';

export const loginUser = (loginData, history) => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.post(`/api/auth/login`, loginData);
    const { token } = response.data;
    localStorage.setItem('jwtToken', token);
    setAuthToken(token);
    const decoded = decode(token);
    // const updateableUserData = {
    //   id: decoded.id,
    //   email: decoded.email,
    //   firstName: decoded.firstName,
    //   lastName: decoded.lastName,
    //   companyName: decoded.companyName,
    //   role: decoded.role,
    //   iat: decoded.iat,
    //   exp: decoded.exp
    // };
    // localStorage.setItem('updateableUserData', JSON.stringify(updateableUserData));
    dispatch(setCurrentUser(decoded));
    dispatch(getSuccessMessage('Succesfully logged in!'));
    dispatch(clearErrors());
    dispatch(isNowLoading(false));
    return history.push('/edit-profile');
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err.response.data.login));
  }
};

// register user
export const registerUser = (registerData, history) => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.post('/api/auth/register', registerData);
    dispatch(isNowLoading(false));
    dispatch(
      getSuccessMessage('Account succesfully created, please log in now!')
    );
    history.push('/login');
    return console.log(response);
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err));
  }
};

// log user out
export const logoutUser = history => dispatch => {
  // remove token from localStorage
  localStorage.removeItem('jwtToken');
  // remove auth header for future requests
  setAuthToken(false);
  // set current user to {} which will also set isAuthenticated to false
  dispatch(setCurrentUser({}));
  dispatch(clearProfile());
  dispatch(clearPlaylist());
  dispatch(clearMessage());

  dispatch(getSuccessMessage('You are logged out'));
  return history.push('/');
};

// set logged in user
export const setCurrentUser = decoded => dispatch => {
  dispatch({
    type: SET_CURRENT_USER,
    payload: decoded
  });
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const getErrorMessage = errorMessage => dispatch => {
  dispatch({
    type: GET_ERRORS,
    payload: errorMessage
  });
  setTimeout(() => {
    dispatch(clearErrors());
  }, 3000);
};

export const clearProfile = () => {
  return {
    type: CLEAR_PROFILE
  };
};

export const clearPlaylist = () => {
  return {
    type: CLEAR_PLAYLIST
  };
};

export const clearMessage = () => {
  return {
    type: CLEAR_MESSAGE
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
