import axios from 'axios';
import {
  IS_LOADING,
  GET_SUCCESS,
  GET_ERRORS,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  GET_ALL_USER_MESSAGE,
  GET_MESSAGE
} from './types';

// get messages by user logged in
export const getAllUserMessage = () => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const findUserMessages = await axios.get('/api/messages/all');
    dispatch({
      type: GET_ALL_USER_MESSAGE,
      payload: findUserMessages.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(
      getErrorMessage(`Server error cannot retrieve user's messages`)
    );
  }
};

// get message by it's id
export const getMessage = messageId => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const findMessage = await axios.get(`/api/messages/${messageId}`);
    dispatch({
      type: GET_MESSAGE,
      payload: findMessage.data
    });
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return getErrorMessage(err.message);
  }
};

// send message
export const sendMessage = (messageData, profileUserId) => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    await axios.post(`/api/messages/${profileUserId}`, messageData);
    dispatch(isNowLoading(false));
    return dispatch(getSuccessMessage('Succesfully send the message'));
  } catch (err) {
    dispatch(isNowLoading(false));
    return getErrorMessage(err.response.message);
  }
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const getErrorMessage = errorMessage => dispatch => {
  dispatch({
    type: GET_ERRORS,
    payload: errorMessage
  });
  setTimeout(() => {
    dispatch(clearErrors());
  }, 3000);
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
