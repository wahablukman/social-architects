import axios from 'axios';
import {
  IS_LOADING,
  GET_ERRORS,
  GET_SUCCESS,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  GET_ALL_USER_PLAYLIST,
  GET_PLAYLIST,
  CREATE_PLAYLIST,
  ADD_PLAYLIST_PHOTO,
  DELETE_PHOTO_FROM_PLAYLIST,
  DELETE_PLAYLIST
} from './types';

// add a photo to a playlist
export const addPhotoToPlaylist = (
  playlistId,
  photosData
) => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const uploadPhotos = await axios.put(
      `/api/playlist/${playlistId}/photos`,
      photosData
    );
    dispatch({
      type: ADD_PLAYLIST_PHOTO,
      payload: uploadPhotos.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {}
};

//create a playlist
export const createPlaylist = playlistData => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const addPlaylist = await axios.post(`/api/playlist`, playlistData);
    dispatch({
      type: CREATE_PLAYLIST,
      payload: addPlaylist.data
    });
    dispatch(getSuccessMessage('playlist created'));
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err));
  }
};

// get all of user's playlists by URL parameter ID
export const getAllUserPlaylistByParamId = paramId => async dispatch => {
  dispatch(isNowLoading(true));

  try {
    const playlists = await axios.get(`/api/playlist/user/${paramId}`);
    dispatch({
      type: GET_ALL_USER_PLAYLIST,
      payload: playlists.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err));
  }
};

// get a user's playlist by req.user (logged in user)
export const getAllUserPlaylistByLoggedInUser = () => async dispatch => {
  dispatch(isNowLoading(true));

  try {
    const playlists = await axios.get(`/api/playlist/user`);
    dispatch({
      type: GET_ALL_USER_PLAYLIST,
      payload: playlists.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    return dispatch(isNowLoading(false));
  }
};

// get a playlist by URL parameter ID
export const getPlaylistById = playlistParamId => async dispatch => {
  dispatch(isNowLoading(true));

  try {
    const playlist = await axios.get(`/api/playlist/${playlistParamId}`);
    dispatch({
      type: GET_PLAYLIST,
      payload: playlist.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err));
  }
};

//delete a playlist
export const deletePlaylist = playlistId => async dispatch => {
  dispatch(isNowLoading(true));

  try {
    await axios.delete(`/api/playlist/${playlistId}`);
    dispatch({
      type: DELETE_PLAYLIST,
      payload: playlistId
    });
    dispatch(isNowLoading(false));
    return dispatch(getSuccessMessage('playlist deleted'));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(
      getErrorMessage(
        'There is an error in deleting the playlist, please try again in a bit'
      )
    );
  }
};

// delete a photo from a playlist
export const deletePhotoFromPlaylist = (
  cloudinaryPhotoId,
  photoId,
  playlistId
) => async dispatch => {
  await dispatch(isNowLoading(true));

  try {
    await axios.delete(
      `/api/playlist/${playlistId}/photos/${photoId}`,
      photoId
    );
    await dispatch({
      type: DELETE_PHOTO_FROM_PLAYLIST,
      payload: cloudinaryPhotoId
    });
    dispatch(getSuccessMessage('Photo deleted'));
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(
      getErrorMessage(
        'unable to delete the photo at the moment, please try again in a bit'
      )
    );
  }
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const getErrorMessage = errorMessage => dispatch => {
  dispatch({
    type: GET_ERRORS,
    payload: errorMessage
  });
  setTimeout(() => {
    dispatch(clearErrors());
  }, 3000);
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
