import {
  GET_ALL_USER_MESSAGE,
  GET_MESSAGE,
  DELETE_MESSAGE,
  CLEAR_MESSAGE
} from '../actions/types';

const initialState = {
  messages: [],
  message: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_USER_MESSAGE:
      return {
        ...state,
        messages: action.payload
      };
    case GET_MESSAGE:
      return {
        ...state,
        message: action.payload
      };
    case CLEAR_MESSAGE:
      return {
        ...state,
        messages: [],
        message: {}
      };
    case DELETE_MESSAGE:
      return state;
    default:
      return state;
  }
};
