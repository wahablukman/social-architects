import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import loadingReducer from './loadingReducer';
import successReducer from './successReducer';
import profileReducer from './profileReducer';
import playlistReducer from './playlistReducer';
import messagesReducer from './messagesReducer';

export default combineReducers({
  auth: authReducer,
  profile: profileReducer,
  playlist: playlistReducer,
  message: messagesReducer,
  error: errorReducer,
  success: successReducer,
  isLoading: loadingReducer
});
