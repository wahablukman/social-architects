import {
  GET_PROFILE,
  GET_ALL_PROFILES,
  UPDATE_AVATAR,
  CLEAR_PROFILE,
  CREATE_OR_UPDATE_PROFILE,
  UPDATE_AUTH_INSIDE_PROFILE
} from '../actions/types';

const initialState = {
  profiles: [],
  profile: {
    _id: '',
    user: {
      _id: '',
      handle: '',
      companyName: '',
      email: ''
    },
    avatar: '',
    avatarId: '',
    socialMedia: [],
    role: '',
    website: '',
    bio: '',
    locations: [],
    createdAt: '',
    updatedAt: ''
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_AUTH_INSIDE_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          user: {
            ...state.profile.user,
            companyName: action.payload.companyName
          }
        }
      };
    case CREATE_OR_UPDATE_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          _id: action.payload._id,
          user: action.payload.user,
          avatar: action.payload.avatar,
          avatarId: action.payload.avatarId,
          socialMedia: action.payload.socialMedia,
          role: action.payload.role,
          website: action.payload.website,
          bio: action.payload.bio,
          locations: action.payload.locations,
          createdAt: action.payload.createdAt,
          updatedAt: action.payload.updatedAt
        }
      };
    case GET_ALL_PROFILES:
      return { ...state, profiles: action.payload };
    case GET_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          _id: action.payload._id,
          user: {
            ...state.profile.user,
            _id: action.payload.user._id,
            handle: action.payload.user.handle,
            companyName: action.payload.user.companyName,
            email: action.payload.user.email
          },
          avatar: action.payload.avatar,
          avatarId: action.payload.avatarId,
          socialMedia: action.payload.socialMedia,
          role: action.payload.role,
          website: action.payload.website,
          bio: action.payload.bio,
          locations: action.payload.locations,
          createdAt: action.payload.createdAt,
          updatedAt: action.payload.updatedAt
        }
      };
    case CLEAR_PROFILE:
      return {
        ...state,
        profiles: [],
        profile: {
          ...state.profile,
          _id: '',
          user: {
            _id: '',
            handle: '',
            companyName: '',
            email: ''
          },
          avatar: '',
          avatarId: '',
          socialMedia: [],
          role: '',
          website: '',
          bio: '',
          locations: [],
          createdAt: '',
          updatedAt: ''
        }
      };
    case UPDATE_AVATAR:
      return {
        ...state,
        profile: {
          ...state.profile,
          avatar: action.payload.avatar,
          avatarId: action.payload.avatarId
        }
      };
    default:
      return state;
  }
};
