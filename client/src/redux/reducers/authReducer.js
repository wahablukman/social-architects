import { SET_CURRENT_USER, UPDATE_AUTH_FROM_PROFILE } from '../actions/types';
import isEmpty from '../../utils/isEmpty';

const initialState = {
  isAuthenticated: false,
  user: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    case UPDATE_AUTH_FROM_PROFILE:
      return {
        ...state,
        user: {
          ...state.user,
          companyName: action.payload.companyName
        }
      };
    default:
      return state;
  }
};
