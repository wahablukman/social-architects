import {
  GET_PLAYLIST,
  GET_ALL_USER_PLAYLIST,
  CREATE_PLAYLIST,
  ADD_PLAYLIST_PHOTO,
  DELETE_PHOTO_FROM_PLAYLIST,
  CLEAR_PLAYLIST,
  DELETE_PLAYLIST
  // ADD_PLAYLIST_PHOTO,
  // DELETE_PLAYLIST_PHOTO
} from '../actions/types';

const initialState = {
  playlists: [],
  playlist: {
    _id: '',
    title: '',
    user: {},
    photos: [],
    createdAt: '',
    updatedAt: ''
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PLAYLIST:
      return {
        ...state,
        playlist: {
          _id: action.payload._id,
          title: action.payload.title,
          user: action.payload.user,
          photos: action.payload.photos,
          createdAt: action.payload.createdAt,
          updatedAt: action.payload.updatedAt
        }
      };
    case GET_ALL_USER_PLAYLIST:
      return {
        ...state,
        playlists: action.payload
      };
    case DELETE_PHOTO_FROM_PLAYLIST:
      return {
        ...state,
        playlist: {
          ...state.playlist,
          photos: state.playlist.photos.filter(
            photo => photo.imageId !== action.payload
          )
        }
      };
    case CREATE_PLAYLIST:
      return {
        ...state,
        playlists: [action.payload, ...state.playlists]
      };
    case DELETE_PLAYLIST:
      return {
        ...state,
        playlists: state.playlists.filter(playlist => {
          return playlist._id !== action.payload;
        })
      };
    case ADD_PLAYLIST_PHOTO:
      return {
        ...state,
        playlist: {
          photos: [action.payload, ...state.playlist.photos]
        }
      };
    case CLEAR_PLAYLIST:
      return {
        playlists: [],
        playlist: {
          _id: '',
          title: '',
          user: {},
          photos: [],
          createdAt: '',
          updatedAt: ''
        }
      };
    default:
      return state;
  }
};
