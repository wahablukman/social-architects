import React, { Component } from 'react';
import { Route, withRouter, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import './assets/css/main.scss';
import setAuthToken from './utils/setAuthToken';
import decode from 'jwt-decode';
import store from './redux/store';
import { setCurrentUser, logoutUser } from './redux/actions/authActions';
import PrivateRoute from './components/common/PrivateRoute';

import Navbar from './components/public/layout/Navbar';
import Homepage from './components/public/Homepage';
import Login from './components/public/auth/Login';
import Logout from './components/public/auth/Logout';
import Register from './components/public/auth/Register';
import Profiles from './components/public/profile/Profiles';
import SingleProfile from './components/public/profile/SingleProfile';
import EditProfile from './components/authenticated/profile/EditProfile';
import Playlist from './components/architects/playlist/Playlist';
import SinglePlaylist from './components/architects/playlist/SinglePlaylist';
import Messages from './components/authenticated/messages/Messages';

//check for token
if (localStorage.jwtToken) {
  // set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // decode token and get user info and exp
  const decoded = decode(localStorage.jwtToken);
  //set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // check for expired tokens
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    //logoutUser
    store.dispatch(logoutUser());
    // clear current profile
    // store.dispatch(clearCurrentProfile());
    //redirect to login
    window.location.href = 'login';
  }
}

class App extends Component {
  render() {
    const { success, error, isLoading } = this.props;
    return (
      <div className="App">
        <Navbar />
        {isLoading ? <div className="loading-spinner">Loading</div> : null}

        <div className="content">
          {success ? (
            <div className="alert alert-success" role="alert">
              {success}
            </div>
          ) : null}
          {error ? (
            // <div className="error-message">
            //   <div className="error-text">{error}</div>
            // </div>
            <div className="alert alert-danger" role="alert">
              {error}
            </div>
          ) : null}
          <Route exact path="/" component={Homepage} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route path="/register" component={Register} />
          <Route exact path="/profiles" component={Profiles} />
          <Route path="/profiles/:id" component={SingleProfile} />
          <Switch>
            <PrivateRoute path="/edit-profile" component={EditProfile} />
          </Switch>
          <Switch>
            <PrivateRoute exact path="/my-playlist" component={Playlist} />
          </Switch>
          <Switch>
            <PrivateRoute
              path="/my-playlist/:playlist_id"
              component={SinglePlaylist}
            />
          </Switch>
          <Switch>
            <PrivateRoute path="/my-messages" component={Messages} />
          </Switch>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error,
  success: state.success,
  isLoading: state.isLoading
});

export default withRouter(connect(mapStateToProps)(App));
