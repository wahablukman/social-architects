import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as authActions from '../../../redux/actions/authActions';

import TextFieldGroup from '../../common/TextFieldGroup';

class Login extends Component {
  state = {
    email: '',
    password: ''
  };

  componentDidMount() {
    const { isAuthenticated } = this.props.auth;
    const { history } = this.props;
    if (isAuthenticated) {
      history.push('/dashboard');
    }
  }

  onChangeInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { loginUser, history } = this.props;
    const loginData = {
      email: this.state.email,
      password: this.state.password
    };

    loginUser(loginData, history);
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6 ml-auto mr-auto mt-5">
            <h1>Login</h1>
            <br />
            <form onSubmit={this.onSubmit}>
              <TextFieldGroup
                label="Email"
                name="email"
                placeholder="email@address.com"
                type="email"
                onChange={this.onChangeInput}
                value={this.state.email}
              />
              <TextFieldGroup
                label="Password"
                name="password"
                placeholder="Very secret-y password"
                type="password"
                onChange={this.onChangeInput}
                value={this.state.password}
              />
              <br />
              <button className="btn btn-primary">Login here</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  success: state.success
});

export default connect(
  mapStateToProps,
  authActions
)(Login);
