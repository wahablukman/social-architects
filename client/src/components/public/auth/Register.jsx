import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as authActions from '../../../redux/actions/authActions';

import TextFieldGroup from '../../common/TextFieldGroup';
import InputRadio from '../../common/InputRadio';

class Register extends Component {
  state = {
    handle: '',
    companyName: '',
    email: '',
    password: '',
    password2: '',
    role: 'Architect'
  };

  onChangeInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onBlurUsername = () => {
    const trimHandle = this.state.handle.trim();
    const removeWhiteSpace = trimHandle.replace(/\s+/g, '');
    const lowercaseHandle = removeWhiteSpace.toLowerCase();
    this.setState({
      handle: lowercaseHandle
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { history, registerUser } = this.props;
    let registerData = new FormData();
    registerData.append('handle', this.state.handle);
    registerData.append('companyName', this.state.companyName);
    registerData.append('email', this.state.email);
    registerData.append('password', this.state.password);
    registerData.append('role', this.state.role);

    registerUser(registerData, history);
  };

  render() {
    return (
      <section className="register-section">
        <div className="container">
          <div className="row">
            <div className="col-md-6 mr-auto ml-auto mt-5">
              <h1>Register</h1>
              <br />
              <form onSubmit={this.onSubmit}>
                <label>Sign up as:</label>
                <br />
                <InputRadio
                  name="role"
                  label="Architect"
                  value="Architect"
                  onChange={this.onChangeInput}
                  checked={this.state.role === 'Architect'}
                />
                <InputRadio
                  name="role"
                  label="General"
                  value="General"
                  onChange={this.onChangeInput}
                  checked={this.state.role === 'General'}
                />
                <br />
                <br />
                <TextFieldGroup
                  label="Username"
                  name="handle"
                  placeholder="yourusername"
                  type="text"
                  onChange={this.onChangeInput}
                  onBlur={this.onBlurUsername}
                  value={this.state.handle}
                  info={`no spaces or uppercase allowed`}
                />
                <TextFieldGroup
                  label="Company / representative name"
                  name="companyName"
                  placeholder="Either one of company name or representative name"
                  type="text"
                  onChange={this.onChangeInput}
                  value={this.state.companyName}
                />
                <TextFieldGroup
                  label="Email"
                  name="email"
                  placeholder="email@address.com"
                  type="email"
                  onChange={this.onChangeInput}
                  value={this.state.email}
                />
                <TextFieldGroup
                  label="Password"
                  name="password"
                  placeholder="Very secret-y password"
                  type="password"
                  onChange={this.onChangeInput}
                  value={this.state.password}
                />
                <TextFieldGroup
                  label="Confirm Password"
                  name="password2"
                  placeholder="Confirm your secret-y password"
                  type="password"
                  onChange={this.onChangeInput}
                  value={this.state.password2}
                />
                <br />
                <button className="btn btn-primary" disabled={false}>
                  Register
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error,
  success: state.success
});

export default withRouter(
  connect(
    mapStateToProps,
    authActions
  )(Register)
);
