import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as authActions from '../../../redux/actions/authActions';

class Logout extends Component {
  state = {};
  componentDidMount() {
    const { logoutUser, history } = this.props;
    logoutUser(history);
  }

  render() {
    return (
      <div>
        <h1>Logout</h1>
      </div>
    );
  }
}

export default connect(
  null,
  authActions
)(Logout);
