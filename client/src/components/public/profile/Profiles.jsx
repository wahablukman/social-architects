import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as profileActions from '../../../redux/actions/profileAction';

import ProfileItem from './ProfileItem';
import ProfileFilters from './ProfileFilters';

class Profiles extends Component {
  state = {};

  componentDidMount = async () => {
    const { getAllProfiles } = this.props;
    await getAllProfiles();
  };

  render() {
    // console.log(this.props);
    // const architects = this.props.profiles.filter(
    //   profile => profile.user.role === 'Architect'
    // );
    const architectProfiles = this.props.profiles.map(
      (architectProfile, index) => {
        return <ProfileItem key={index} architectProfile={architectProfile} />;
      }
    );
    return (
      <div className="container profiles-container">
        <h1 className="col-md-12 pt-5 pb-5 text-center">Profiles</h1>
        <div className="row">
          <ProfileFilters />
          <div className="profiles col-md-10 container">
            <div className="row">{architectProfiles}</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  profiles: state.profile.profiles
});

export default connect(
  mapStateToProps,
  profileActions
)(Profiles);
