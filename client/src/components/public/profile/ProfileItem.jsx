import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class ProfileItem extends Component {
  state = {};
  render() {
    const { architectProfile } = this.props;
    return (
      <div className="col-md-4 profile card-wrapper">
        <div className="card">
          <div className="image">
            <img src={architectProfile.avatar} alt="" />
          </div>
          <div className="card-body">
            <h5 className="card-title">
              {architectProfile.user ? architectProfile.user.companyName : null}
            </h5>
            {architectProfile.user ? (
              <NavLink
                to={`/profiles/${architectProfile.user._id}`}
                className="btn btn-primary"
              >
                See profile
              </NavLink>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default ProfileItem;
