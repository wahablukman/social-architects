import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getProfile } from '../../../redux/actions/profileAction';
import { sendMessage } from '../../../redux/actions/messagesActions';
import {
  getAllUserPlaylistByParamId,
  getPlaylistById
} from '../../../redux/actions/playlistAction';

import PlaylistItem from '../../architects/playlist/PlaylistItem';
import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaFieldGroup from '../../common/TextAreaFieldGroup';
import BootstrapModal from '../../common/BootstrapModal';
// import isEmpty from '../../../utils/isEmpty';

class SingleProfile extends Component {
  state = {
    name: '',
    email: '',
    message: '',
    messageModal: false,
    playlistModal: false,
    playlistPhotos: []
  };

  componentDidMount = async () => {
    const { getProfile, getAllUserPlaylistByParamId } = await this.props;
    const paramID = await this.props.match.params.id;
    await getProfile(paramID);
    await getAllUserPlaylistByParamId(paramID);
  };

  onClickToggleModal = () => {
    this.setState({
      messageModal: !this.state.messageModal
    });
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      playlistPhotos: nextProps.playlist.photos
    });
  }

  onClickTogglePlaylist = async playlistId => {
    const { getPlaylistById } = this.props;
    await getPlaylistById(playlistId);
    await this.setState({
      playlistModal: !this.state.playlistModal
    });
  };

  onChangeInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    const { profile, sendMessage } = this.props;
    const userProfileId = profile.user._id;
    e.preventDefault();
    const messageData = {
      name: this.state.name,
      email: this.state.email,
      message: this.state.message
    };

    await sendMessage(messageData, userProfileId);

    await this.onClickToggleModal();
  };

  render() {
    const { profile } = this.props;
    let { playlists } = this.props;

    playlists = playlists.filter(playlist => playlist.photos.length > 0);
    playlists = playlists.map((playlist, index) => {
      return (
        <PlaylistItem
          playlist={playlist}
          onClickTogglePlaylist={() => this.onClickTogglePlaylist(playlist._id)}
          key={index}
          isEditingOwnPlaylist={false}
        />
      );
    });
    return (
      <div className="single-profile">
        {this.state.messageModal ? (
          <BootstrapModal
            isFormModal={true}
            onSubmit={this.onSubmit}
            onClickToggleModal={this.onClickToggleModal}
            playlistTitle={`Send message to ${profile.user.companyName}`}
          >
            <TextFieldGroup
              label="Name"
              name="name"
              type="text"
              onChange={this.onChangeInput}
              value={this.state.name}
            />
            <TextFieldGroup
              label="Email"
              name="email"
              type="email"
              onChange={this.onChangeInput}
              value={this.state.email}
            />
            <TextAreaFieldGroup
              label="Message"
              name="message"
              type="text"
              onChange={this.onChangeInput}
              value={this.state.message}
            />
          </BootstrapModal>
        ) : null}

        {this.state.playlistModal ? (
          <BootstrapModal
            playlistTitle="Playlist photos"
            isFormModal={false}
            onClickToggleModal={() =>
              this.setState({
                playlistModal: !this.state.playlistModal
              })
            }
          >
            {this.state.playlistPhotos.map((playlistPhoto, index) => {
              return <img key={index} src={playlistPhoto.imageUrl} alt="" />;
            })}
          </BootstrapModal>
        ) : null}

        <div className="profile-picture-image">
          <div
            className="bg-image"
            style={{
              backgroundImage: `url(${profile.avatar ? profile.avatar : null})`
            }}
          />
          <div className="profile-picture">
            <img src={profile.avatar ? profile.avatar : null} alt="" />
          </div>
        </div>

        <div className="container">
          <div className="row">
            <div className="profile-details col-md-8 m-auto">
              {profile.user ? (
                <>
                  <div className="top-profile-details d-inline-block">
                    <div className="location top-profile-detail d-inline-block">
                      <i className="fas fa-map-marker-alt" /> Jakarta
                    </div>
                    <div
                      className="message top-profile-detail d-inline-block"
                      onClick={this.onClickToggleModal}
                    >
                      <i className="fas fa-envelope" /> Message
                    </div>
                  </div>
                  <h1 className="company-name mb-4">
                    {profile.user.companyName}
                  </h1>
                  <p className="company-bio">{profile.bio}</p>{' '}
                </>
              ) : null}

              <h1 className="mt-5 text-left mb-4">Playlists</h1>
              <div className="playlists-section">
                <div className="row">{playlists}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.profile.profile,
  playlists: state.playlist.playlists,
  playlist: state.playlist.playlist
});

export default connect(
  mapStateToProps,
  { getProfile, sendMessage, getAllUserPlaylistByParamId, getPlaylistById }
)(SingleProfile);
