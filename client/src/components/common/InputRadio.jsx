import React from 'react';
import classnames from 'classnames';

const InputRadio = ({ name, checked, value, label, onChange }) => {
  return (
    <div className="btn-group btn-group-toggle mr-1 mb-1" data-toggle="buttons">
      <label
        className={classnames('btn btn-outline-info', {
          active: checked
        })}
      >
        <input
          type="radio"
          name={name}
          id={label}
          onChange={onChange}
          autoComplete="off"
          checked={checked}
          value={value}
        />{' '}
        {label}
      </label>
    </div>
  );
};

export default InputRadio;
