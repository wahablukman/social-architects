import React, { Component } from 'react';

class BootstrapModal extends Component {
  state = {};
  render() {
    const {
      onClickToggleModal,
      isFormModal,
      onSubmit,
      playlistTitle
    } = this.props;
    return (
      <div>
        <div className="message-modal">
          {isFormModal ? (
            <form onSubmit={onSubmit}>
              <div className="modal" tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">{playlistTitle}</h5>
                      <button
                        type="button"
                        className="close"
                        onClick={onClickToggleModal}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                    <div className="modal-body">{this.props.children}</div>

                    <div className="modal-footer">
                      {isFormModal ? (
                        <button type="submit" className="btn btn-primary">
                          Send
                        </button>
                      ) : null}

                      <button
                        type="button"
                        className="btn btn-secondary"
                        onClick={onClickToggleModal}
                      >
                        Close
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          ) : (
            <div className="modal" tabIndex="-1" role="dialog">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Playlist photos</h5>
                    <button
                      type="button"
                      className="close"
                      onClick={onClickToggleModal}
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>

                  <div className="modal-body">{this.props.children}</div>

                  <div className="modal-footer">
                    {isFormModal ? (
                      <button type="submit" className="btn btn-primary">
                        Send
                      </button>
                    ) : null}

                    <button
                      type="button"
                      className="btn btn-secondary"
                      onClick={onClickToggleModal}
                    >
                      Close
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default BootstrapModal;
