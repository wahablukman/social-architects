import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

class TextFieldGroup extends Component {
  state = {};
  render() {
    const {
      name,
      label,
      type,
      placeholder,
      value,
      onChange,
      onBlur,
      disabled,
      error,
      info,
      hasPrepend
    } = this.props;
    const prepend = (
      <div className="input-group-prepend">
        <div className="input-group-text">
          <i className={`fab fa-${hasPrepend}`} />
        </div>
      </div>
    );
    return (
      <div className="input-group form-group">
        <label htmlFor={name} style={{ width: '100%' }}>
          {label}
        </label>
        {hasPrepend ? prepend : null}
        <input
          type={type}
          className={classnames('form-control form-control-lg', {
            'is-invalid': error
          })}
          placeholder={placeholder}
          name={name}
          value={value}
          onChange={onChange}
          disabled={disabled}
          onBlur={onBlur}
        />
        {info && <small className="form-text text-muted">{info}</small>}
        {error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  }
}

TextFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  info: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.string
};

TextFieldGroup.defaultProps = {
  type: 'text'
};

export default TextFieldGroup;
