import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

class TextAreaFieldGroup extends Component {
  state = {};
  render() {
    const {
      name,
      placeholder,
      value,
      onChange,
      error,
      info,
      label
    } = this.props;
    return (
      <div className="form-group">
        <label>{label}</label>
        <textarea
          className={classnames('form-control form-control-lg', {
            'is-invalid': error
          })}
          placeholder={placeholder}
          name={name}
          value={value}
          onChange={onChange}
          rows="5"
        />
        {info && <small className="form-text text-muted">{info}</small>}
        {error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  }
}

TextAreaFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  info: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

export default TextAreaFieldGroup;
