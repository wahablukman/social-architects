import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as playlistActions from '../../../redux/actions/playlistAction';
// import { NavLink } from 'react-router-dom';

import TextFieldGroup from '../../common/TextFieldGroup';
// import isEmpty from '../../../utils/isEmpty';
import PlaylistItem from './PlaylistItem';

class Playlist extends Component {
  state = {
    title: ''
  };

  componentDidMount() {
    const { getAllUserPlaylistByLoggedInUser } = this.props;
    getAllUserPlaylistByLoggedInUser();
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onClickDelete = playlistId => {
    const { deletePlaylist } = this.props;
    deletePlaylist(playlistId);
  };

  onSubmit = async e => {
    e.preventDefault();
    const { createPlaylist } = this.props;

    const playlistData = {
      title: this.state.title
    };
    await createPlaylist(playlistData);
    this.setState({
      title: ''
    });
  };

  render() {
    let { playlists } = this.props;
    playlists = playlists.map((playlist, index) => {
      return (
        <PlaylistItem
          playlist={playlist}
          onClickDelete={() => this.onClickDelete(playlist._id)}
          key={index}
          isEditingOwnPlaylist={true}
        />
      );
    });
    return (
      <div className="playlists-section pt-5">
        <div className="container">
          <h1 className="mb-4 text-center">All Playlist</h1>
          {/* <TextFieldGroup /> */}
          <div className="col-md-12">
            <form className="mb-5" onSubmit={this.onSubmit}>
              <div className="d-inline-block">
                <TextFieldGroup
                  label="New Playlist"
                  type="text"
                  name="title"
                  value={this.state.title}
                  onChange={this.onChange}
                />
              </div>
              <br />
              <button type="submit" className="btn btn-primary">
                Create new playlist
              </button>
            </form>
          </div>
          <div className="playlists col-md-12">
            <div className="row">
              {playlists.length > 0 ? (
                playlists
              ) : (
                <div className="empty-info">
                  You have not created a playlist yet, do so by filling the form
                  above
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  playlists: state.playlist.playlists
});

export default connect(
  mapStateToProps,
  playlistActions
)(Playlist);
