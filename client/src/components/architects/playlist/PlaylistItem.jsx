import React, { Component } from 'react';
import isEmpty from '../../../utils/isEmpty';
import { NavLink } from 'react-router-dom';

class PlaylistItem extends Component {
  state = {};
  render() {
    // console.log(this.props);
    const {
      playlist,
      onClickDelete,
      isEditingOwnPlaylist,
      onClickTogglePlaylist
    } = this.props;
    return (
      <div className="playlist col-md-4 mb-4 card-wrapper">
        <div className="card">
          <div className="card-body">
            <div className="top-playlist-photo-wrapper">
              {!isEmpty(playlist.photos) ? (
                <img
                  className="top-playlist-photo"
                  src={playlist.photos[0].imageUrl}
                  alt=" "
                />
              ) : null}
            </div>

            {isEditingOwnPlaylist ? (
              <>
                <NavLink to={`/my-playlist/${playlist._id}`}>
                  {playlist.title}
                </NavLink>
                <br />
              </>
            ) : (
              <h6>{playlist.title}</h6>
            )}

            {isEditingOwnPlaylist ? (
              <small>{`${playlist.photos.length} photos`}</small>
            ) : (
              <small onClick={onClickTogglePlaylist}>{`${
                playlist.photos.length
              } photos`}</small>
            )}

            {isEditingOwnPlaylist ? (
              <button
                className="btn btn-danger delete-button"
                onClick={onClickDelete}
              >
                Delete
              </button>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistItem;
