import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as playlistActions from '../../../redux/actions/playlistAction';
import Dropzone from 'react-dropzone';

class SinglePlaylist extends Component {
  state = {};

  onDropChange = async photos => {
    const {
      addPhotoToPlaylist,
      getPlaylistById,
      getSuccessMessage
    } = this.props;
    const { playlist_id } = this.props.match.params;

    let photoData = new FormData();
    for (let i = 0; i < photos.length; i++) {
      photoData.set('photo', photos[i]);
      await addPhotoToPlaylist(playlist_id, photoData);
    }

    await getPlaylistById(playlist_id);

    await getSuccessMessage('all photos uploaded and playlist updated');
  };

  onSubmit = e => {
    e.preventDefault();
  };

  onClickDelete = (cloudinaryPhotoId, photoId, playlistId) => {
    // console.log(cloudinaryPhotoId, photoId, playlistId);
    const { deletePhotoFromPlaylist } = this.props;
    deletePhotoFromPlaylist(cloudinaryPhotoId, photoId, playlistId);
  };

  componentDidMount() {
    const { getPlaylistById } = this.props;
    const { playlist_id } = this.props.match.params;
    getPlaylistById(playlist_id);
  }

  render() {
    const { playlist } = this.props;
    return (
      <div className="single-playlist-section pt-5">
        <div className="container">
          <h1 className="mb-5">{playlist.title}</h1>
          {/* eslint-disable-next-line */}
          <form className="mb-5" onSubmit={this.onSubmit}>
            <label>Upload photo(s) to this playlist</label>
            <div className="dropzone">
              <Dropzone
                accept="image/*"
                name="avatar"
                multiple={true}
                onDrop={this.onDropChange}
              >
                <i className="fas fa-cloud-upload-alt" />
                <h5>Click here / drag your photos here</h5>
              </Dropzone>
            </div>
          </form>
          <div className="playlist-photos">
            <div className="row">
              {playlist.photos.length > 0 ? (
                playlist.photos.map((playlistPhoto, index) => {
                  return (
                    <div className="playlist-photo col-md-2 mb-5" key={index}>
                      <img src={playlistPhoto.imageUrl} alt="" />
                      <button
                        onClick={() =>
                          this.onClickDelete(
                            playlistPhoto.imageId,
                            playlistPhoto._id,
                            playlist._id
                          )
                        }
                        className="btn btn-danger delete-photo"
                      >
                        Delete
                      </button>
                    </div>
                  );
                })
              ) : (
                <div className="empty-info">
                  You dont have photo(s) uploaded to this playlist yet
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  playlist: state.playlist.playlist
});

export default connect(
  mapStateToProps,
  playlistActions
)(SinglePlaylist);
