import React, { Component } from 'react';
import Moment from 'react-moment';

class MessageItem extends Component {
  state = {};
  render() {
    const { message, onClick } = this.props;
    return (
      <div>
        <div className="message-header" onClick={onClick}>
          <h6 className="message-name">{message.name}</h6>
          <div className="d-none message">{message.message}</div>
          <small className="message-date">
            <Moment className="moment-date" format="DD MMM, YYYY">
              {message.createdAt}
            </Moment>
          </small>
        </div>
      </div>
    );
  }
}

export default MessageItem;
