import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as messageActions from '../../../redux/actions/messagesActions';
import isEmpty from '../../../utils/isEmpty';
import MessageItem from './MessageItem';
import DisplaySingleMessage from './DisplaySingleMessage';

class Messages extends Component {
  state = {
    name: '',
    email: '',
    date: '',
    message: ''
  };

  onClickGetMessage = async messageId => {
    const { getMessage } = this.props;
    await getMessage(messageId);
  };

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps.message)) {
      this.setState({
        name: nextProps.message.name,
        email: nextProps.message.email,
        date: nextProps.message.date,
        message: nextProps.message.message
      });
    }
  }

  componentDidMount() {
    const { getAllUserMessage } = this.props;
    getAllUserMessage();
  }

  render() {
    let { messages } = this.props;
    messages = messages.map((message, index) => {
      return (
        <MessageItem
          message={message}
          onClick={() => this.onClickGetMessage(message._id)}
          key={index}
        />
      );
    });
    return (
      <div className="messages-section">
        <div className="container pt-5">
          <h1>Messages</h1>
          <br />
          <br />
          <div className="row">
            <div className="col-md-3 message-headers">{messages}</div>
            <div className="col-md-9 display-message">
              {this.state.name ? (
                <DisplaySingleMessage
                  name={this.state.name}
                  email={this.state.email}
                  date={this.state.date}
                  message={this.state.message}
                />
              ) : (
                <div className="empty-info">Message will be displayed here</div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.message.messages,
  message: state.message.message
});

export default connect(
  mapStateToProps,
  messageActions
)(Messages);
