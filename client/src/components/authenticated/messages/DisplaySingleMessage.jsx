import React, { Component } from 'react';
import Moment from 'react-moment';

class DisplaySingleMessage extends Component {
  state = {};
  render() {
    const { name, email, date, message } = this.props;
    return (
      <>
        <h3 className="name">{name !== '' ? name : null}</h3>
        <p className="email mt-3 mb-1">{email !== '' ? email : null}</p>
        <br />
        <small className="date">
          {date !== '' ? (
            <Moment className="moment-date" format="DD MMM, YYYY">
              {date}
            </Moment>
          ) : null}
        </small>
        <div className="message pt-4">{message ? message : null}</div>
      </>
    );
  }
}

export default DisplaySingleMessage;
