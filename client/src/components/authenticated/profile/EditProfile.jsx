import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';
import * as profileActions from '../../../redux/actions/profileAction';
import store from '../../../redux/store';

import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaFieldGroup from '../../common/TextAreaFieldGroup';
import isEmpty from '../../../utils/isEmpty';
import { getSuccessMessage } from '../../../redux/actions/playlistAction';

class EditProfile extends Component {
  state = {
    companyName: '',
    facebook: '',
    twitter: '',
    instagram: '',
    youtube: '',
    linkedin: '',
    website: '',
    bio: '',
    avatar: '',
    avatarPreview: ''
  };

  componentDidMount = async () => {
    const { getCurrentUserProfile } = this.props;
    await getCurrentUserProfile();
  };

  componentWillReceiveProps = async nextProps => {
    if (!isEmpty(nextProps.profile._id)) {
      await this.setState({
        facebook: nextProps.profile.socialMedia.facebook,
        twitter: nextProps.profile.socialMedia.twitter,
        instagram: nextProps.profile.socialMedia.instagram,
        youtube: nextProps.profile.socialMedia.youtube,
        linkedin: nextProps.profile.socialMedia.linkedin,
        website: nextProps.profile.website,
        bio: nextProps.profile.bio,
        avatarPreview: nextProps.profile.avatar,
        companyName: nextProps.profile.user.companyName
      });
    }
  };

  onDropChange = file => {
    this.setState(
      {
        avatar: file[0]
      },
      () => {
        this.handleImagePreview(this.state.avatar);
      }
    );
  };

  handleImagePreview = avatar => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          avatarPreview: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(avatar);
  };

  onChangeInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    e.preventDefault();
    const { createOrUpdateProfile, updateAvatar } = this.props;

    let profileData = new FormData();
    profileData.append('companyName', this.state.companyName);
    profileData.append('bio', this.state.bio);
    profileData.append('website', this.state.website);
    profileData.append('facebook', this.state.facebook);
    profileData.append('twitter', this.state.twitter);
    profileData.append('instagram', this.state.instagram);
    profileData.append('youtube', this.state.youtube);
    profileData.append('linkedin', this.state.linkedin);

    if (this.state.bio) {
      await createOrUpdateProfile(profileData);
    }

    if (this.state.avatar !== '') {
      let avatarData = new FormData();
      avatarData.append('avatar', this.state.avatar);
      await updateAvatar(avatarData);
      store.dispatch(getSuccessMessage('Profile succesfully updated'));
    }
  };

  render() {
    console.log(this.state);
    return (
      <div className="container">
        <div className="row">
          <div className="create-profile col-md-12">
            <div className="col-md-6 mr-auto ml-auto mt-5">
              <h1>Edit Your Profile</h1>
              <br />
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  label="Company Name"
                  name="companyName"
                  onChange={this.onChangeInput}
                  value={this.state.companyName}
                />
                <label>Your Profile Picture</label>
                {this.state.avatarPreview !== '' ? (
                  <div className="avatar-preview-wrapper">
                    <img
                      className="avatar-preview"
                      src={this.state.avatarPreview}
                      alt=""
                    />
                  </div>
                ) : null}

                <div className="dropzone">
                  <Dropzone
                    accept="image/*"
                    name="avatar"
                    multiple={false}
                    onDrop={this.onDropChange}
                  >
                    <i className="fas fa-cloud-upload-alt" />
                    <h5>Click here / drag your profile picture here</h5>
                  </Dropzone>
                </div>

                <TextAreaFieldGroup
                  label="Bio"
                  name="bio"
                  placeholder="Fill your bio here"
                  type="text"
                  onChange={this.onChangeInput}
                  value={this.state.bio}
                />

                <TextFieldGroup
                  label="Website"
                  name="website"
                  placeholder="Website URL"
                  type="text"
                  onChange={this.onChangeInput}
                  value={this.state.website}
                />

                <TextFieldGroup
                  label="Facebook"
                  name="facebook"
                  placeholder="Facebook URL"
                  type="text"
                  hasPrepend="facebook"
                  onChange={this.onChangeInput}
                  value={this.state.facebook}
                />
                <TextFieldGroup
                  label="Twitter"
                  name="twitter"
                  placeholder="Twitter URL"
                  type="text"
                  hasPrepend="twitter"
                  onChange={this.onChangeInput}
                  value={this.state.twitter}
                />
                <TextFieldGroup
                  label="Instagram"
                  name="instagram"
                  placeholder="Instagram URL"
                  type="text"
                  hasPrepend="instagram"
                  onChange={this.onChangeInput}
                  value={this.state.instagram}
                />
                <TextFieldGroup
                  label="YouTube"
                  name="youtube"
                  placeholder="YouTube URL"
                  type="text"
                  hasPrepend="youtube"
                  onChange={this.onChangeInput}
                  value={this.state.youtube}
                />
                <TextFieldGroup
                  label="LinkedIn"
                  name="linkedin"
                  hasPrepend="linkedin"
                  placeholder="LinkedIn URL"
                  type="text"
                  onChange={this.onChangeInput}
                  value={this.state.linkedin}
                />
                <br />
                <button type="submit" className="btn btn-primary">
                  Update Profile
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth.user,
  profile: state.profile.profile
});

export default connect(
  mapStateToProps,
  profileActions
)(EditProfile);
