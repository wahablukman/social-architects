import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';
import * as profileActions from '../../../redux/actions/profileAction';

import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaFieldGroup from '../../common/TextAreaFieldGroup';
import isEmpty from '../../../utils/isEmpty';

class EditProfile extends Component {
  state = {
    facebook: '',
    twitter: '',
    instagram: '',
    youtube: '',
    linkedin: '',
    website: '',
    bio: '',
    avatar: [],
    avatarPreview: []
  };

  componentDidMount() {}

  onDropChange = file => {
    console.log(file);
    file.map(eachFile => {
      return this.setState(
        {
          avatar: [...this.state.avatar, eachFile]
        },
        () => {
          this.handleImagePreview(this.state.avatar);
          console.log(this.state.avatar);
        }
      );
    });
  };

  handleImagePreview = avatar => {
    //convert to base64 for preview only, real image is stored in cloudinary
    avatar.map(eachAvatar => {
      const reader = new FileReader();
      reader.addEventListener(
        'load',
        () => {
          // console.log(myFileItemReader.result)
          const prevImageConversion = reader.result;
          this.setState({
            avatarPreview: [...this.state.avatarPreview, prevImageConversion]
          });
        },
        false
      );
      reader.readAsDataURL(eachAvatar);
      console.log(this.state);
    });
  };

  onChangeInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    e.preventDefault();
    const { createOrUpdateProfile, updateAvatar } = this.props;

    let profileData = new FormData();
    profileData.append('bio', this.state.bio);
    profileData.append('website', this.state.website);
    profileData.append('facebook', this.state.facebook);
    profileData.append('twitter', this.state.twitter);
    profileData.append('instagram', this.state.instagram);
    profileData.append('youtube', this.state.youtube);
    profileData.append('linkedin', this.state.linkedin);

    if (this.state.bio) {
      await createOrUpdateProfile(profileData);
    }

    if (this.state.avatarPreview) {
      let avatarData = new FormData();
      avatarData.append('avatar', this.state.avatar);
      await updateAvatar(avatarData);
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="create-profile col-md-12">
            <div className="col-md-6 mr-auto ml-auto mt-5">
              <h1>Create Your Profile</h1>
              <br />
              <form onSubmit={this.onSubmit}>
                <label>Your Profile Picture</label>
                {!isEmpty(this.state.avatarPreview)
                  ? this.state.avatarPreview.map(eachPreview => {
                      return (
                        <div className="avatar-preview-wrapper">
                          <img
                            className="avatar-preview"
                            src={eachPreview}
                            alt=""
                          />
                        </div>
                      );
                    })
                  : null}

                <div className="dropzone">
                  <Dropzone
                    accept="image/*"
                    name="avatar"
                    onDrop={this.onDropChange}
                  >
                    <i className="fas fa-cloud-upload-alt" />
                    <h5>Click here / drag your profile picture here</h5>
                  </Dropzone>
                </div>

                <TextAreaFieldGroup
                  label="Bio"
                  name="bio"
                  placeholder="Fill your bio here"
                  type="text"
                  onChange={this.onChangeInput}
                  value={this.state.bio}
                />

                <TextFieldGroup
                  label="Website"
                  name="website"
                  placeholder="Website URL"
                  type="text"
                  onChange={this.onChangeInput}
                  value={this.state.website}
                />

                <TextFieldGroup
                  label="Facebook"
                  name="facebook"
                  placeholder="Facebook URL"
                  type="text"
                  hasPrepend="facebook"
                  onChange={this.onChangeInput}
                  value={this.state.facebook}
                />
                <TextFieldGroup
                  label="Twitter"
                  name="twitter"
                  placeholder="Twitter URL"
                  type="text"
                  hasPrepend="twitter"
                  onChange={this.onChangeInput}
                  value={this.state.twitter}
                />
                <TextFieldGroup
                  label="Instagram"
                  name="instagram"
                  placeholder="Instagram URL"
                  type="text"
                  hasPrepend="instagram"
                  onChange={this.onChangeInput}
                  value={this.state.instagram}
                />
                <TextFieldGroup
                  label="YouTube"
                  name="youtube"
                  placeholder="YouTube URL"
                  type="text"
                  hasPrepend="youtube"
                  onChange={this.onChangeInput}
                  value={this.state.youtube}
                />
                <TextFieldGroup
                  label="LinkedIn"
                  name="linkedin"
                  hasPrepend="linkedin"
                  placeholder="LinkedIn URL"
                  type="text"
                  onChange={this.onChangeInput}
                  value={this.state.linkedin}
                />
                <br />
                <button type="submit" className="btn btn-primary">
                  Create Profile
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.profile.profile
});

export default connect(
  mapStateToProps,
  profileActions
)(EditProfile);
