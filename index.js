const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const port = process.env.PORT || 5000;
const passport = require('passport');
const multipart = require('connect-multiparty');
const path = require('path');

//routes
const auth = require('./routes/api/auth');
const profile = require('./routes/api/profile');
const playlist = require('./routes/api/playlist');
const message = require('./routes/api/message');

//body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(multipart());

// DB Config
const db = require('./config/keys/keys').monguURI;

// Connect to mongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true }
  )
  .then(() => {
    console.log('mongodb connected');
    // console.log(res);
  })
  .catch(err => {
    console.log(err);
  });

// passport middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);

// declaring routes for easier use
app.use('/api/auth', auth);
app.use('/api/profile', profile);
app.use('/api/playlist', playlist);
app.use('/api/messages', message);

// Server static assets if in production
if (process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

//starting development server
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
