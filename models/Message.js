const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'auth'
    },
    registeredUser: {
      type: Boolean,
      required: true
    },
    registeredUserId: {
      type: String
    },
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      require: true
    },
    phone: {
      type: String
    },
    message: {
      type: String
    }
  },
  { timestamps: true }
);

module.exports = Message = mongoose.model('message', MessageSchema);
