const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlaylistSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'auth'
    },
    title: {
      type: String,
      required: true
    },
    photos: [
      {
        imageUrl: {
          type: String
        },
        imageId: {
          type: String
        }
      },
      { timestamps: true }
    ]
  },
  { timestamps: true }
);

module.exports = Playlist = mongoose.model('playlist', PlaylistSchema);
