const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AuthSchema = new Schema(
  {
    handle: {
      type: String,
      required: true
    },
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    companyName: {
      type: String
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = Auth = mongoose.model('auth', AuthSchema);
