const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ExperienceSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'auth'
    },
    title: {
      type: String,
      required: true
    },
    company: {
      type: String,
      required: true
    },
    location: {
      type: String
    },
    from: {
      type: Date,
      required: true
    },
    to: {
      type: Date
    },
    current: {
      type: Boolean,
      default: false
    },
    description: {
      type: String
    }
  },
  { timestamps: true }
);

module.exports = Experience = mongoose.model('experience', ExperienceSchema);
