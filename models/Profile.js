const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProfileSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'auth'
    },
    avatar: {
      type: String,
      default: '/default-images/avatar-default.jpg'
    },
    avatarId: {
      type: String,
      default: 'avatar-default'
    },
    socialMedia: {
      youtube: {
        type: String
      },
      twitter: {
        type: String
      },
      facebook: {
        type: String
      },
      linkedin: {
        type: String
      },
      instagram: {
        type: String
      }
    },
    website: {
      type: String
    },
    role: {
      type: String
    },
    locations: [
      {
        city: {
          type: String
        },
        street: {
          type: String
        },
        postCode: {
          type: String
        },
        latitude: {
          type: String
        },
        longitude: {
          type: String
        }
      }
    ],
    bio: {
      type: String
    }
  },
  { timestamps: true }
);

module.exports = Profile = mongoose.model('profile', ProfileSchema);
