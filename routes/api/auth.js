const express = require('express');
const router = express.Router();
const keys = require('../../config/keys/keys');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const cloudinary = require('cloudinary');
const isEmpty = require('../../validation/isEmpty');

cloudinary.config({
  cloud_name: keys.cloudinary_cloud_name,
  api_key: keys.cloudinary_api_key,
  api_secret: keys.cloudinary_api_secret
});

// load models required
const Auth = require('../../models/Auth');
const Profile = require('../../models/Profile');

// @route   GET api/auth/test
// @desc    test users route
// @access  Public
router.get('/test', (req, res) => {
  res.json({ msg: 'users works' });
});

// @route   GET api/auth/all
// @desc    get all users
// @access  Public
router.get('/all', async (req, res) => {
  let errors = {};
  try {
    const findUsers = await Auth.find();
    return res.json(findUsers);
  } catch (err) {
    errors.message = 'Server error, cannot get users';
    return res.status(500).json(errors);
  }
});

// @route   POST api/auth/register
// @desc    registering user to database
// @access  Public
router.post('/register', async (req, res) => {
  let errors = {};

  const handle = req.body.handle;
  const handleTrim = handle.trim();
  const handleUppercase = handleTrim.toLowerCase();

  const newUser = new Auth({
    handle: handleUppercase,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    companyName: req.body.companyName,
    email: req.body.email,
    password: req.body.password,
    role: req.body.role
  });

  const profileData = new Profile({
    socialMedia: {
      youtube: req.body.youtube || '',
      facebook: req.body.facebook || '',
      instagram: req.body.instagram || '',
      linkedin: req.body.linkedin || '',
      twitter: req.body.twitter || ''
    },
    website: req.body.website || '',
    bio: req.body.bio || ''
  });

  try {
    const emailExists = await Auth.findOne({ email: req.body.email });
    const handleExists = await Auth.findOne({ handle: req.body.handle });
    if (emailExists) {
      errors.email = 'Email is already taken';
      return res.status(400).json(errors);
    } else if (handleExists) {
      errors.handle = 'Username is already taken';
      return res.status(400).json(errors);
    } else {
      if (!isEmpty(req.files.avatar)) {
        try {
          const cloudinaryOptions = {
            overwrite: true,
            folder: 'avatar'
          };
          const avatarImage = await cloudinary.v2.uploader.upload(
            req.files.avatar.path,
            cloudinaryOptions
          );
          newUser.avatar = avatarImage.url;
          newUser.avatarId = avatarImage.public_id;
        } catch (err) {
          errors.cloudinary =
            'Cloudinary error, cannot upload your image, please try again later';
          return res.status(500).json(errors);
        }
      }
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, async (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          try {
            const registerUser = await newUser.save();
            // return res.json(registerUser);
            profileData.user = registerUser._id;
            profileData.role = registerUser.role;
            const registerProfile = await profileData.save();
            return res.json({ registerUser, registerProfile });
          } catch (err) {
            errors.register =
              'Server error, cannot register, please try again in a bit';
            return res.status(500).json(errors);
          }
        });
      });
    }
  } catch (err) {
    errors.server = 'Server error';
    return res.status(500).json(errors);
  }
});

// @route   POST api/auth/login
// @desc    Logging in user and handling auth
// @access  Public
router.post('/login', async (req, res) => {
  let errors = {};

  const email = req.body.email;
  const password = req.body.password;

  try {
    const emailExists = await Auth.findOne({ email });
    if (!emailExists) {
      errors.login = 'Your email is not registered with us yet';
      return res.status(404).json(errors);
    } else {
      try {
        const isMatch = await bcrypt.compare(password, emailExists.password);
        if (isMatch) {
          // User matched
          // create jwt payload
          // return res.json(emailExists);
          const payload = {
            id: emailExists._id,
            handle: emailExists.handle,
            firstName: emailExists.firstName,
            lastName: emailExists.lastName,
            companyName: emailExists.companyName,
            avatar: emailExists.avatar,
            avatarId: emailExists.avatarId,
            role: emailExists.role
          };

          // Sign token - 604800 = a week
          jwt.sign(
            payload,
            keys.secretOrKey,
            { expiresIn: 604800 },
            (err, token) => {
              res.json({
                success: true,
                token: 'Bearer ' + token
              });
            }
          );
        } else {
          errors.password = 'password incorrect';
          return res.status(400).json(errors);
        }
      } catch (err) {
        return res.status(500).json(err);
      }
    }
  } catch (err) {
    return res.status(500).json(err);
  }
});

// @route   GET api/auth/current-user
// @desc    retrieve current user info
// @access  Private
router.get(
  '/current-user',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    const findUser = await Auth.findOne({ _id: req.user.id });
    res.json(findUser);
    // res.json({
    //   id: auth.id,
    //   name: req.user.name,
    //   email: req.user.email
    // });
  }
);

// @route   GET api/auth/current-user/:decoded_id
// @desc    retrieve current user info from jwt decoded id
// @access  Private
router.get(
  '/current-user/:decoded_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    const findUser = await Auth.findOne({ _id: req.params.decoded_id });
    res.json(findUser);
    // res.json({
    //   id: auth.id,
    //   name: req.user.name,
    //   email: req.user.email
    // });
  }
);

module.exports = router;
