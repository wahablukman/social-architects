const express = require('express');
const router = express.Router();
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty');

//load require models
const Message = require('../../models/Message');

// @route   GET api/messages/all
// @desc    Retrieve all user's messages
// @access  Private
router.get(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    try {
      const findMessages = await Message.find({ user: req.user._id });
      return res.json(findMessages);
      // if (!isEmpty(findMessages)) {
      //   return res.json(findMessages);
      // } else {
      //   errors.message = 'You have 0 message';
      //   return res.status(404).json(errors);
      // }
    } catch (err) {
      errors.message = 'Server error';
      return res.status(500).json(errors);
    }
  }
);

// @route   GET api/messages/:message_id
// @desc    Retrieve user's message by ID
// @access  Private
router.get(
  '/:message_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    try {
      const findMessage = await Message.findOne({ _id: req.params.message_id });
      if (!isEmpty(findMessage)) {
        return res.json(findMessage);
      } else {
        return res.status(404).json('no post found');
      }
    } catch (err) {
      errors.message = 'Server error cannot get the message at the moment';
      return res.status(500).json(errors);
    }
  }
);

// @route   POST api/messages/:profile_user_id
// @desc    Send a message to authenticated user
// @access  Public
router.post('/:profile_user_id', async (req, res) => {
  let errors = {};

  const messageData = new Message({
    user: req.params.profile_user_id,
    registeredUser: req.body.registeredUser || false,
    name: req.body.name,
    email: req.body.email,
    message: req.body.message
  });

  // return res.json(req.user);
  if (messageData.registeredUser === true) {
    messageData.registeredUserId = req.body.registeredUserId;
  }

  try {
    const saveMessage = await messageData.save();
    return res.json(saveMessage);
  } catch (err) {
    errors.message = 'Server error, cannot send your message to this user';
    return res.status(500).json(errors);
  }
});

module.exports = router;
