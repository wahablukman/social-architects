const express = require('express');
const router = express.Router();
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty');
const cloudinary = require('cloudinary');
const keys = require('../../config/keys/keys');

cloudinary.config({
  cloud_name: keys.cloudinary_cloud_name,
  api_key: keys.cloudinary_api_key,
  api_secret: keys.cloudinary_api_secret
});

//load require models
const Profile = require('../../models/Profile');
const Auth = require('../../models/Auth');

// @route   POST api/profile
// @desc    create or update (if existed) profile
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    const authData = {
      companyName: req.body.companyName
    };
    const profileData = {
      user: req.user._id,
      socialMedia: {
        youtube: req.body.youtube,
        facebook: req.body.facebook,
        instagram: req.body.instagram,
        linkedin: req.body.linkedin,
        twitter: req.body.twitter
      },
      role: req.user.role,
      website: req.body.website,
      bio: req.body.bio
    };

    try {
      const updateUser = await Auth.findOneAndUpdate(
        { _id: req.user.id },
        { $set: authData },
        { new: true }
      );
      const profileExists = await Profile.findOne({ user: req.user._id });
      if (profileExists) {
        // update profile if profile already exists
        // check if the person editing is authorised (prevent authenticated user to delete other user's stuffs)
        if (profileExists.user.toString() === req.user._id.toString()) {
          const updateProfile = await Profile.findOneAndUpdate(
            {
              user: req.user._id
            },
            { $set: profileData },
            { new: true }
          ).populate('user');

          return res.json({ updateProfile, updateUser });
        } else {
          errors.message = 'you are not authorised to delete this playlist';
          return res.status(401).json(errors);
        }
      } else {
        //create new profile
        try {
          const createUser = await new Profile(profileData).save();
          return res.json(createUser);
        } catch (err) {
          errors.message =
            'Server error, cannot create your profile, please try again in a bit';
          return res.status(500).json(errors);
        }
      }
    } catch (err) {
      errors.message = 'Server error cannot update your user stuffs';
      return res.status(500).json(errors);
    }
  }
);

// @route   POST api/profile/profile-picture
// @desc    upload profile picture to cloudinary
// @access  Private
router.post(
  '/profile-picture',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const profile = await Profile.findOne({ user: req.user._id });
    // return res.json(profile);

    if (!isEmpty(req.files.avatar)) {
      const cloudinaryOptions = {
        overwrite: true,
        folder: 'avatar'
      };
      if (profile.avatarId !== 'avatar-default') {
        // delete old image and upload the new one
        await cloudinary.v2.uploader.destroy(profile.avatarId);
      }
      const uploadAvatar = await cloudinary.v2.uploader.upload(
        req.files.avatar.path,
        cloudinaryOptions
      );
      profile.avatar = uploadAvatar.url;
      profile.avatarId = uploadAvatar.public_id;
      const saveUpdatedAvatar = await profile.save();
      return res.json(saveUpdatedAvatar);
    } else {
      return res.json(profile);
    }
  }
);

// @route   GET api/profile/all
// @desc    Find user profiles
// @access  Public
router.get('/all', async (req, res) => {
  let errors = {};

  try {
    const allProfile = await Profile.find().populate('user');
    return res.json(allProfile);
  } catch (err) {
    errors.message = 'Server error';
    res.status(500).json(errors);
  }
});

// @route   GET api/profile/myprofile
// @desc    Find current profile
// @access  Private
router.get(
  '/myprofile',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    try {
      const findProfile = await Profile.findOne({
        user: req.user._id
      }).populate('user');
      return res.json(findProfile);
    } catch (err) {
      errors.message =
        'Server error, cannot find your profile, please try again in a bit';
      return res.status(500).json(errors);
    }
  }
);

// @route   GET api/profile/:id
// @desc    Find single user profile
// @access  Public
router.get('/:id', async (req, res) => {
  let errors = {};

  try {
    const profile = await Profile.findOne({ user: req.params.id }).populate(
      'user'
    );
    if (profile) {
      return res.json(profile);
    } else {
      errors.profile = 'not a single profile found';
      return res.status(404).json(errors);
    }
  } catch (err) {
    errors.message =
      'Server error, cannot find your profile, please try again in a bit';
    res.status(500).json(errors);
  }
});

// @route   POST api/profile/location
// @desc    create available location
// @access  Private
router.post(
  '/location',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const newLocation = {
      city: req.body.city
    };

    try {
      const profileExists = await Profile.findOne({ user: req.user._id });
      if (profileExists) {
        profileExists.locations.unshift(newLocation);
        try {
          const updateProfile = await profileExists.save();
          return res.json(updateProfile);
        } catch (err) {
          errors.server =
            'Server error, cannot update your location, please try again in a bit';
          return res.status(500).json(errors);
        }
      } else {
        errors.profile =
          'Profile not found, you have to make a profile first before adding location';
        return res.status(500).json(errors);
      }
    } catch (err) {
      errors.server = 'Server error';
      return res.status(500).json(errors);
    }
  }
);

module.exports = router;
