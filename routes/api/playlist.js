const express = require('express');
const router = express.Router();
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty');
const cloudinary = require('cloudinary');
const keys = require('../../config/keys/keys');

// load required models
const Playlist = require('../../models/Playlist');

cloudinary.config({
  cloud_name: keys.cloudinary_cloud_name,
  api_key: keys.cloudinary_api_key,
  api_secret: keys.cloudinary_api_secret
});

// @route   GET api/playlist/all
// @desc    Retrieve all playlist
// @access  Public
router.get('/all', async (req, res) => {
  let errors = {};
  try {
    const findAllPlaylists = await Playlist.find().populate('user');
    return res.json(findAllPlaylists);
  } catch (err) {
    errors.server = 'Server error';
    return res.status(500).json(errors);
  }
});

// @route   GET api/playlist/user/:user_id
// @desc    Retrieve all playlist of a user
// @access  Public
router.get('/user/:user_id', async (req, res) => {
  let errors = {};
  try {
    const findAllPlaylists = await Playlist.find({
      user: req.params.user_id
    })
      .populate('user')
      .sort({ createdAt: -1 });
    return res.json(findAllPlaylists);
  } catch (err) {
    errors.server = 'Server error';
    return res.status(500).json(errors);
  }
});

// @route   GET api/playlist/user
// @desc    Retrieve all playlist of a user by logged in ID
// @access  Private
router.get(
  '/user',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    try {
      const findAllPlaylists = await Playlist.find({
        user: req.user._id
      })
        .populate('user')
        .sort({ createdAt: -1 });
      return res.json(findAllPlaylists);
    } catch (err) {
      errors.server = 'Server error';
      return res.status(500).json(errors);
    }
  }
);

// @route   GET api/playlist/:id
// @desc    Retrieve a playlist by ID
// @access  Public
router.get('/:id', async (req, res) => {
  let errors = {};
  try {
    const findPlaylist = await Playlist.findOne({
      _id: req.params.id
    }).populate('user');
    if (!isEmpty(findPlaylist)) {
      return res.json(findPlaylist);
    } else {
      errors.playlist = 'no playlist found';
      return res.status(404).json(errors);
    }
  } catch (err) {
    errors.server = 'Server error';
    return res.status(500).json(errors);
  }
});

// @route   DELETE api/playlist/:id
// @desc    DELETE a playlist by ID / as well as delete photos from cloudinary
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const findPlaylist = await Playlist.findOne({ _id: req.params.id });
    if (findPlaylist) {
      // console.log(findPlaylist.user, req.user._id);
      // return res.json(findPlaylist.photos.length);
      // check if the person editing is authorised (prevent authenticated user to delete other user's stuffs)
      if (findPlaylist.user.toString() === req.user._id.toString()) {
        // check if the playlist have photos inside, if so, delete it from cloudinary and database
        if (findPlaylist.photos.length > 0) {
          await cloudinary.v2.api.delete_resources_by_prefix(
            `playlist/${findPlaylist._id}`
          );
        }
        await findPlaylist.remove();
        return res.json({ success: 'success deleting' });
      } else {
        return res
          .status(401)
          .json('you are not authorised to delete this playlist');
      }
    } else {
      return res.status(404).json('no playlist found to be deleted');
    }
  }
);

// @route   PUT api/playlist/:playlist_id/photos
// @desc    Create a photo inside playlist
// @access  Private
router.put(
  '/:playlist_id/photos',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    try {
      const findPlaylist = await Playlist.findOne({
        _id: req.params.playlist_id
      });
      // return res.json(profile);
      //check if we can find playlist
      if (!isEmpty(findPlaylist)) {
        // check if the person editing is authorised (prevent authenticated user to delete other user's stuffs)
        if (findPlaylist.user.toString() === req.user._id.toString()) {
          //check if there is / are photo input files
          if (!isEmpty(req.files.photo)) {
            const cloudinaryOptions = {
              overwrite: true,
              folder: `playlist/${findPlaylist._id}`
            };
            const uploadPhoto = await cloudinary.v2.uploader.upload(
              req.files.photo.path,
              cloudinaryOptions
            );
            const newPhoto = {
              imageUrl: uploadPhoto.url,
              imageId: uploadPhoto.public_id
            };
            findPlaylist.photos.unshift(newPhoto);
            const saveUpdatedPlaylist = await findPlaylist.save();
            return res.json(saveUpdatedPlaylist);
          } else {
            return res.json(findPlaylist);
          }
        } else {
          return res
            .status(401)
            .json('you are not upload photo to this playlist');
        }
      } else {
        errors.playlist = 'no playlist found with that id';
        return res.status(404).json(errors);
      }
    } catch (err) {
      errors.server = 'Server error';
      return res.status(500).json(errors);
    }
  }
);

// @route   DELETE api/playlist/:playlist_id/photos/:photo_id
// @desc    Delete a photo inside a playlist
// @access  Private
router.delete(
  '/:playlist_id/photos/:photo_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const findPlaylist = await Playlist.findOne({
      _id: req.params.playlist_id
    });
    const playlistPhoto = findPlaylist.photos.filter(playlistPhoto => {
      return playlistPhoto._id.toString() === req.params.photo_id;
    });

    //check if the playlist has photo(s)
    if (!isEmpty(playlistPhoto)) {
      // check if the person editing is authorised (prevent authenticated user to delete other user's stuffs)
      if (findPlaylist.user.toString() === req.user._id.toString()) {
        const removeIndex = findPlaylist.photos
          .map(photo => {
            return photo._id.toString();
          })
          .indexOf(req.params.photo_id);

        await cloudinary.v2.uploader.destroy(
          findPlaylist.photos[removeIndex].imageId
        );

        findPlaylist.photos.splice(removeIndex, 1);
        const saveUpdatedPlaylist = await findPlaylist.save();
        return res.json(saveUpdatedPlaylist);
      } else {
        return res
          .status(401)
          .json('you are not authorised to delete this photo');
      }
    } else {
      errors.photo = 'photo not found';
      return res.status(404).json(errors);
    }
  }
);

// @route   POST api/playlist
// @desc    Create a playlist
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const playlistData = new Playlist({
      user: req.user._id,
      title: req.body.title
    });

    try {
      const savePlaylist = await playlistData.save();
      return res.json(savePlaylist);
    } catch (err) {
      errors.server = 'Server error';
      return res.status(500).json(errors);
    }
  }
);

module.exports = router;
